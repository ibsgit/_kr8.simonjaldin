<?php
function save_log($user = NULL, $tipe, $aksi)
{

    $session = \Config\Services::session();
    if ($user == NULL) {
        $user = $session->userinfo->id;
    }
    $param['log_user'] = $user;
    $param['log_jenis'] = $tipe; //login, logout, ST, SPD
    $param['log_detail'] = is_array($aksi) ? json_encode($aksi) : $aksi;
    $model_log = new \App\Models\model_log;
    $model_log->insert($param);
}
