<div id="kt_quick_panel" class="offcanvas offcanvas-left pt-5 pb-10">

    <!--begin::Content-->
    <div class="offcanvas-content px-10">
        <div class="navi navi-icon-circle navi-spacer-x-0">
            <a href="<?php echo base_url('referensi/pegawai'); ?>" class="navi-item">
                <div class="navi-link rounded">
                    <div class="symbol symbol-50 mr-3">
                        <div class="symbol-label"><i class="fas fa-users text-success icon-lg"></i></div>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold font-size-lg">Referensi Pegawai</div>
                    </div>
                </div>
            </a>
            <a href="<?php echo base_url('referensi/mak'); ?>" class="navi-item">
                <div class="navi-link rounded">
                    <div class="symbol symbol-50 mr-3">
                        <div class="symbol-label"><i class="fas fa-clipboard-list text-warning icon-lg"></i></div>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold font-size-lg">Referensi MAK</div>
                    </div>
                </div>
            </a>
            <!-- <a href="<?php echo base_url('referensi/kabkota'); ?>" class="navi-item">
                <div class="navi-link rounded">
                    <div class="symbol symbol-50 mr-3">
                        <div class="symbol-label"><i class="fas fa-map-marked text-danger icon-lg"></i></div>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold font-size-lg">Referensi Kabupaten/Kota</div>
                    </div>
                </div>
            </a> -->
            <!-- <a href="<?php echo base_url('referensi/sbm'); ?>" class="navi-item">
                <div class="navi-link rounded">
                    <div class="symbol symbol-50 mr-3">
                        <div class="symbol-label"><i class="fas fa-money-bill-wave-alt text-primary icon-lg"></i></div>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold font-size-lg">Referensi Standar Biaya Masukan</div>
                    </div>
                </div>
            </a>
            <a href="<?php echo base_url('referensi/st'); ?>" class="navi-item">
                <div class="navi-link rounded">
                    <div class="symbol symbol-50 mr-3">
                        <div class="symbol-label"><i class="far fa-newspaper text-warning icon-lg"></i></div>
                    </div>
                    <div class="navi-text">
                        <div class="font-weight-bold font-size-lg">Referensi Surat Tugas</div>
                    </div>
                </div>
            </a> -->

        </div>
    </div>
    <!--end::Content-->
</div>