<?php echo view('index/0-top'); ?>
<style>
    span.text {
        font-family: 'Courier New', Courier, monospace !important;
    }

    a.dropdown-item {
        padding: 0px 10px !important;
    }

    .line-number {
        background: url(<?php echo  base_url('public/img/line-number.png'); ?>);
        background-size: 30px 27010px;
        background-attachment: local;
        background-repeat: no-repeat;
        background-position: -10px -10px;
        padding-left: 25px !important;
        padding-top: 3px !important;
        font-size: 12px !important;
        border-color: #ccc;
    }

    textarea {
        resize: none;
    }

    .highlight_tertugas {
        color: red !important;
        font-weight: bold !important;
    }

    .highlight_tujuan {
        background: yellow !important;
    }
</style>

<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="fas fa-users-cog text-primary"></i>
            </span>
            <h3 class="card-label"><?php echo $page; ?></h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <button type="button" class="btn btn-primary tambahST">
                <i class="fas fa-folder-plus"></i>
                Tambah ST
            </button>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <!-- <div class="table-responsive"> -->
        <table class="table table-bordered table-hover table-checkable" id="kt_datatable">
            <thead>
                <tr>
                    <th style="width: 10%;">Nomor & Tanggal</th>
                    <th style="width: 20%">Kegiatan</th>
                    <th style="width: 30%;">Tujuan</th>
                    <th style="width: 25%;">Pegawai</th>
                    <th style="width: 15%;">Aksi</th>
                </tr>
            </thead>
        </table>
        <!-- </div> -->
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->

<!-- Modal Tambah ST-->
<div class="modal fade" id="myFormModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form" id="myForm">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label class="col-lg-5 col-form-label">Nomor Urut Surat</label>
                                <div class="col-lg-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-sort-numeric-down text-success"></i>
                                            </span>
                                        </div>
                                        <input name="urutan_st" type="text" class="form-control form-control-sm" placeholder="Otomatis" />
                                        <input name="id_st_1" type="hidden" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-5 col-form-label">Tanggal Surat</label>
                                <div class="col-lg-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-calendar-alt text-success"></i>
                                            </span>
                                        </div>
                                        <input name="tgl_st" type="text" class="form-control form-control-sm" id="datepicker_tgl_st" placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Detail-->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <form class="form" id="detailForm">
                <div class="modal-header">
                </div>
                <div class="modal-body pb-2">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="exampleTextarea">Header
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="id_st_5" type="hidden" />
                                <textarea wrap="off" class="form-control" name="header_st" rows="2" style="font-size:11px;margin-top: 0px; margin-bottom: 0px; height: 50px;"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="exampleTextarea">Jabatan Spesimen
                                    <span class="text-danger">*</span>
                                </label>
                                <textarea wrap="off" class="form-control" name="jabatan_spesimen" rows="2" style="font-size:11px;margin-top: 0px; margin-bottom: 0px; height: 50px;"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="exampleTextarea">Nama Spesimen
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="nama_spesimen" style="font-size:11px;" />
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="exampleTextarea">NIP Spesimen
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="nip_spesimen" style="font-size:11px;" />
                            </div>
                        </div>
                    </div>
                    <hr style="border: 0.1rem solid black;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleTextarea">Dasar
                                    <span class="text-danger">*</span>
                                </label>
                                <textarea wrap="off" class="form-control line-number" name="dasar_st" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 200px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleTextarea">Untuk
                                    <span class="text-danger">*</span>
                                </label>
                                <textarea wrap="off" class="form-control line-number" name="peruntukan_st" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 75px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <hr style="border: 0.1rem solid black;">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleTextarea">DIPA
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="nama_dipa" style="font-size:11px;" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="exampleTextarea">Nomor DIPA
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="no_dipa" style="font-size:11px;" />
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="exampleTextarea">Tanggal DIPA
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="tgl_dipa" id="tgl_dipa" style="font-size:11px;" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer py-1">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Kegiatan-->
<div class="modal fade" id="kegiatanModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form" id="kegiatanForm">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleTextarea">Kegiatan
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="id_st_2" type="hidden" />
                                <textarea class="form-control" name="kegiatan_st" id="kegiatan_st" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 104px;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Tujuan-->
<div class="modal fade" id="tujuanModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form" id="tujuanForm">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="offset-lg-2 col-lg-8">
                            <div class="form-group">
                                <label for="exampleTextarea">Kabupaten/Kota
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="id_st_3" type="hidden" />
                                <input name="id_tujuan" type="hidden" />
                                <select class="form-control selectpicker" title="- Pilih Kabupaten/Kota -" data-size="6" data-live-search="true" name="id_kabkota">
                                    <?php foreach ($kabkota as $d) {
                                        echo "<option value='$d->id_kabkota'>$d->nama_prov - $d->nama_kabkota</option>";
                                    }; ?>
                                </select>
                            </div>
                        </div>
                        <div class="offset-lg-2 col-lg-8">
                            <div class="form-group">
                                <label for="exampleTextarea">Detail Tujuan
                                    <span class="text-danger">*</span>
                                </label>
                                <textarea class="form-control" name="detail_tujuan" id="detail_tujuan" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 104px;"></textarea>
                            </div>
                        </div>
                        <div class="offset-lg-2 col-lg-6">
                            <div class="form-group">
                                <label for="exampleTextarea">Tanggal
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="input-daterange input-group" id="datepicker_periode">
                                    <input type="text" class="form-control text-center" name="tgl_awal" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            s.d.
                                        </span>
                                    </div>
                                    <input type="text" class="form-control text-center" name="tgl_akhir" />

                                </div>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            Total :
                                        </span>
                                    </div>
                                    <input type="text" disabled class="form-control text-center" id="totalHari" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            hari
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Tertugas-->
<div class="modal fade" id="tertugasModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form" id="tertugasForm">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <div class="row ">
                        <div class="col-lg-6">
                            <div class="form-group ">
                                <div id="datepicker_tertugas" class=" d-flex justify-content-end"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleTextarea">Pegawai
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="id_st_4" type="hidden" />
                                <input name="id_tertugas" type="hidden" />
                                <select class="form-control selectpicker" title="- Pilih Pegawai -" data-size="6" data-live-search="true" name="nip_pegawai">
                                    <?php foreach ($pegawai as $d) {
                                        echo "<option class value='$d->nipBaru'>$d->nipBaru - $d->nama</option>";
                                    }; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleTextarea">Detail Tujuan
                                    <span class="text-danger">*</span>
                                </label>
                                <select id="opsiTujuan" class="form-control" title="- Pilih Tujuan -" data-size="6" name="id_tujuan">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Viewer-->
<div class="modal fade" id="viewerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row ">
                    <div class="col-lg-12">
                        <iframe id="embedViewer" src="" frameborder="0" width="100%" height="600px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo view('index/i-js-global'); ?>

<!--begin::Page Vendors(used by this page)-->
<script src="<?php echo base_url(); ?>/public/metronic/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<!--end::Page Vendors-->

<!--begin::Page Scripts(used by this page)-->
<script>
    var tanggal_dinas_tertugas = [];
    var tanggal_dinas_tujuan = [];

    function update_highlight_tertugas(nip = null, id_tujuan = null) {
        $('#datepicker_tertugas * td.day').removeClass('highlight_tertugas');
        if (nip != null) {
            id = nip;
            tujuan = id_tujuan;
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/api/datepicker/tertugas_dinas/'; ?>' + id + '/' + tujuan,
                data: {
                    id: id,
                    tujuan: tujuan,
                },
                success: function(result) {
                    tanggal_dinas_tertugas = result;
                    $('#datepicker_tertugas').datepicker('update');
                }
            });
        } else {
            tanggal_dinas_tertugas = [];
        }
    }

    function update_highlight_tujuan(id_tujuan = null) {
        $('#datepicker_tertugas * td.day').removeClass('highlight_tujuan');
        if (id_tujuan != null) {
            id = id_tujuan;
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/api/datepicker/tujuan_dinas/'; ?>' + id,
                data: {
                    id: id,
                },
                success: function(result) {
                    tanggal_dinas_tujuan = result;
                    $('#datepicker_tertugas').datepicker('update');
                    // $('#datepicker_tertugas * td.day').addClass('active');
                }
            });
        } else {
            tanggal_dinas_tujuan = [];
        }
    }

    var datepicker_tertugas = $('#datepicker_tertugas').datepicker({
        daysOfWeekDisabled: '0,1,2,3,4,5,6',
        beforeShowDay: function(date) {
            calender_date = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
            var tertugas_index = $.inArray(calender_date, tanggal_dinas_tertugas);
            var tujuan_index = $.inArray(calender_date, tanggal_dinas_tujuan);
            if (tertugas_index > -1 && tujuan_index > -1) {
                return {
                    classes: 'highlight_tertugas highlight_tujuan',
                };
            }
            if (tertugas_index > -1) {
                return {
                    classes: 'highlight_tertugas',
                };
            }
            if (tujuan_index > -1) {
                return {
                    classes: 'highlight_tujuan',
                };
            }

        }
    });





    $('#tgl_dipa').datepicker();

    var datepicker_periode = $('#datepicker_tgl_st').datepicker({
        autoclose: true,
    });
    var datepicker_periode = $('#datepicker_periode').datepicker({
        autoclose: true,
    }).on('change', function(e) {
        var d1 = $('input[name ="tgl_awal"]').datepicker('getDate'),
            d2 = $('input[name ="tgl_akhir"]').datepicker('getDate'),
            diff = 0;
        if (d1 && d2) {
            diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
            // al`ert(diff);
            $('#totalHari').val(diff + 1);
        }
    });


    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    $(document).ready(function() {
        var oTable = $('#kt_datatable').DataTable({
            pageLength: 5,
            lengthMenu: [
                [5, 10, 20, -1],
                [5, 10, 20, 'All']
            ],
            'order': [
                [0, 'desc']
            ],
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url(); ?>/api/datatables/st",
            columns: [{
                    data: 'no_st',
                    className: 'align-top '
                },
                {
                    data: 'kegiatan_st',
                    className: 'align-top'
                },
                {
                    data: 'id_st',
                    className: 'align-top'
                },
                {
                    data: 'petugas',
                    className: 'align-top'
                },
                {
                    data: 'id_st',
                    className: 'align-top'
                },
            ],
            columnDefs: [{
                    targets: -1,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '\
                        ' + (full.tertugas_st.length > 0 ? '\
                        <div class="btn-group btn-block">\
                        <span class="btn btn-outline-info btn-square print py-1" ds="0"><i class="la la-print px-1"></i><i class="la la-file-o px-1"></i></span>\
                        <span class="btn btn-outline-info btn-square print py-1" ds="1">DS <i class="la la-file-o px-1"></i></span>\
                        </div>\
                        <br/>\
                        <div class="btn-group btn-block">\
                        <span class="btn btn-outline-warning btn-square print2 py-1" ds="0"><i class="la la-print px-1"></i><i class="la la-files-o px-1"></i></span>\
                        <span class="btn btn-outline-warning btn-square print2 py-1" ds="1">DS <i class="la la-files-o px-1"></i></span>\
                        </div>' : '') + '\
                        <span class="btn text-center btn-block float-center my-1 py-1 btn-square btn-outline-danger hapus ">\<i class="la la-trash"></i></span>\
						';
                    },
                }, {
                    targets: 0,
                    render: function(data, type, full, meta) {
                        return '<span class="btn text-left btn-block align-left my-1 py-1 btn-xs btn-square btn-success font-size-xs edit text-secondary">\
                        <strong>' + full.no_st + '<br>' + full.tgl_st + '</strong>\
                        </span>\
                        <span class="btn text-center btn-block float-center my-1 py-1 btn-square btn-outline-success detail ">\<i class="la la-edit"></i> detail</span>\
                        ';
                    },
                }, {
                    targets: 1,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        if (data === null) {
                            return '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon kegiatan">\
                        <i class="la la-edit"></i>\
                        </a>\
                        ';
                        } else {
                            return '<span class="btn text-left btn-block align-left my-1 py-1 btn-xs btn-square btn-primary font-size-xs kegiatan">\
                            ' + (data === null ? '' : data.substring(0, 100)) + (data.length >= 100 ? '...' : '') + '\
                        </span>';
                        }
                    },
                }, {
                    targets: 2,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var string = '<td class="m-0 p-0 align-top"  style="border: none;width:10%;"><a href="javascript:;" class="m-1 btn btn-xs btn-outline-danger btn-square btn-icon tambahTujuan m-0">\
                        <i class="flaticon2-plus  icon-sm"></i>\
                        </a></td>\
                        <td class="m-0 p-0"  style="border: none;">';
                        $.each(full.tujuan_st, function(key, val) {
                            string += '<button type="button" class="close" aria-label="Close"><i aria-hidden="true" id_tujuan="' + val['id_tujuan'] + '" class="flaticon2-delete icon-sm mr-1 hapusTujuan text-dark"></i></button>\
                            <span class="btn btn-block text-left my-1 pl-3 pr-15 py-1 btn-xs btn-square btn-danger font-size-xs tujuan" id_tujuan="' + val['id_tujuan'] + '" id_kabkota="' + val['id_kabkota'] + '"  tgl_awal="' + val['tgl_awal'] + '"  tgl_akhir="' + val['tgl_akhir'] + '"><b class="text-white">' + val['nama_kabkota'] + '</b>\
                            <br>' + val['detail_tujuan'] + '\
                            <br><span><b class="text-white">' + val['tgl_awal'].split("-").reverse().join("-") + '</b> s.d. <b class="text-white">' + val['tgl_akhir'].split("-").reverse().join("-") + '</b> (<b class="text-white">' + ((Math.floor((Date.parse(val['tgl_akhir']) - Date.parse(val['tgl_awal'])) / 86400000 + 1))) + '</b> hari)\
                            </span> \
                            </span> \
                            ';
                        });
                        string += '</td>\
                        ';
                        return '\
                        <table class="table"><tr>' + string + '</tr></table>\
						';
                    },
                }, {
                    targets: 3,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        if (full.tujuan_st.length > 0) {
                            var string = '<td class="m-0 p-0 align-top"  style="border: none;width:10%;"><a href="javascript:;" class="m-1 btn btn-xs btn-outline-warning btn-square btn-icon tambahTertugas m-0">\
                        <i class="flaticon2-plus  icon-sm"></i>\
                        </a></td>\
                        <td class="m-0 p-0"  style="border: none;">';
                            $.each(full.tertugas_st, function(key, val) {
                                string += '<button type="button" class="close" aria-label="Close"><i aria-hidden="true" id_tertugas="' + val['id_tertugas'] + '" class="flaticon2-delete icon-sm mr-1 hapusTertugas text-dark"></i></button>\
                            <span class="text-secondary btn btn-block text-left my-1 pl-3 pr-15 py-1 btn-xs btn-square btn-warning font-size-xs tertugas" id_tertugas="' + val['id_tertugas'] + '" id_tujuan="' + val['id_tujuan'] + '" nip_pegawai="' + val['nip_pegawai'] + '" >\
                            <b class="text-dark">' + val['nama'] + '</b>\
                            <br><b class="text-dark-75">' + val['nama_kabkota'] + '</b>\
                            </span>\
                            </span> \
                            ';
                            });
                            string += '</td>\
                        ';
                            return '\
                            <table class="table"><tr>' + string + '</tr></table>\
                        ';
                        } else {
                            return '';
                        }
                    },
                },

            ],
        });

        $('#kt_datatable tbody').on('click', '.kegiatan', function() {
            var rowData = oTable.row($(this).closest("tr.even,tr.odd")).data();
            // alert(rowData);
            $('#kegiatanModal').modal('show');
            $('input[name ="id_st_2"]').val(rowData.id_st);
            $('textarea[name ="kegiatan_st"]').val(rowData.kegiatan_st);
        });
        $('#kt_datatable tbody').on('click', '.tambahTujuan', function() {
            var rowData = oTable.row($(this).closest("tr.even,tr.odd")).data();
            // alert(rowData);
            $('#tujuanModal').modal('show');
            $('input[name ="id_st_3"]').val(rowData.id_st);
            $('input[name ="id_tujuan"]').val(0);
            $('select[name ="id_kabkota"]').val('default');
            $('select[name ="id_kabkota"]').selectpicker("refresh");
            $('textarea[name ="detail_tujuan"]').val('');
            $('input[name ="tgl_awal"]').val('');
            $('input[name ="tgl_akhir"]').val('');
            $('#datepicker_periode input').val('');
            $('#totalHari').val('0');


        });
        $('#kt_datatable tbody').on('click', '.tujuan', function() {
            var rowData = oTable.row($(this).closest("tr.even,tr.odd")).data();
            var rowVal = $(this).closest("span").html().split('<br>');
            // alert(rowVal[1]);
            $('#tujuanModal').modal('show');
            $('input[name ="id_st_3"]').val(rowData.id_st);
            $('input[name ="id_tujuan"]').val($(this).attr('id_tujuan'));
            $('select[name ="id_kabkota"]').selectpicker('val', $(this).attr('id_kabkota'));
            $('textarea[name ="detail_tujuan"]').val(rowVal[1].trim());
            $('input[name ="tgl_awal"]').datepicker('update', '').datepicker('update', $(this).attr('tgl_awal').split("-").reverse().join("-"));
            $('input[name ="tgl_akhir"]').datepicker('update', '').datepicker('update', $(this).attr('tgl_akhir').split("-").reverse().join("-"));
            $('#datepicker_periode').datepicker('update');
        });


        $('#kt_datatable tbody').on('click', '.tambahTertugas', function() {
            var rowData = oTable.row($(this).closest("tr.even,tr.odd")).data();
            // alert(rowData);
            $('#tertugasModal').modal('show');
            $('input[name ="id_st_4"]').val(rowData.id_st);
            $('input[name ="id_tertugas"]').val(0);
            $('select[name ="nip_pegawai"]').val('default');
            $('select[name ="nip_pegawai"]').selectpicker("refresh");
            $('#opsiTujuan').html('');
            var toAppend = '<option class value="">- Pilih Tujuan -</option>';
            $.each(rowData.tujuan_st, function(i, o) {
                // alert(o.id_tujuan);
                toAppend += '<option value="' + o.id_tujuan + '">' + o.nama_kabkota + '</option>';
            });
            $('#opsiTujuan').append(toAppend);
            $("#opsiTujuan").val();
            update_highlight_tertugas();
            update_highlight_tujuan();
        });

        $('#kt_datatable tbody').on('click', '.tertugas', function() {
            var rowData = oTable.row($(this).closest("tr.even,tr.odd")).data();
            $('#tertugasModal').modal('show');
            $('input[name ="id_st_4"]').val(rowData.id_st);
            $('input[name ="id_tertugas"]').val($(this).attr('id_tertugas'));
            $('#opsiTujuan').html('');
            var toAppend = '<option class value="">- Pilih Tujuan -</option>';
            $.each(rowData.tujuan_st, function(i, o) {
                // alert(o.id_tujuan);
                toAppend += '<option value="' + o.id_tujuan + '">' + o.nama_kabkota + '</option>';
            });
            $('#opsiTujuan').append(toAppend);
            $('select[name ="id_tujuan"]').val($(this).attr('id_tujuan'));
            $('select[name ="nip_pegawai"]').selectpicker("refresh");
            $('select[name ="nip_pegawai"]').selectpicker('val', $(this).attr('nip_pegawai'));
            //datepicker marking dinas
            update_highlight_tertugas($(this).attr('nip_pegawai'));
            update_highlight_tujuan($(this).attr('id_tujuan'));

        });
        $('select[name ="nip_pegawai"]').on('change', function() {
            update_highlight_tertugas($(this).val());
        });

        $('select[name ="id_tujuan"]').on('change', function() {
            update_highlight_tujuan($(this).val());
        });
        $('.tambahST').on('click', function() {
            var rowData = oTable.row($(this).closest("tr")).data();
            // alert(rowData.tgl_st);
            $('#myFormModal').modal('show');
            $('input[name ="id_st_1"]').val(0);
            $('input[name ="urutan_st"]').val('');
            $('input[name ="tgl_st"]').datepicker('update', '<?php echo date('d-m-Y'); ?>');
        });

        $('#kt_datatable tbody').on('click', '.edit', function() {
            var rowData = oTable.row($(this).closest("tr")).data();
            // alert(rowData.tgl_st);
            $('#myFormModal').modal('show');
            $('input[name ="id_st_1"]').val(rowData.id_st);
            $('input[name ="urutan_st"]').val(rowData.urutan_st);
            $('input[name ="tgl_st"]').val(rowData.tgl_st);
            $('#datepicker_tgl_st').datepicker('update', '').datepicker('update', rowData.tgl_st);

        });


        $('#kt_datatable tbody').on('click', '.print', function() {
            var rowData = oTable.row($(this).closest("tr")).data();
            ds = $(this).attr('ds');
            $('#viewerModal').modal('show');
            $('#embedViewer').attr('src', '');
            $('#embedViewer').attr('src', '<?php echo base_url(); ?>/cetak/print_st/tipe1/' + rowData.id_st + '/A4/' + ds, rowData.id_st);
        });


        $('#kt_datatable tbody').on('click', '.print2', function() {
            var rowData = oTable.row($(this).closest("tr")).data();
            ds = $(this).attr('ds');
            $('#viewerModal').modal('show');
            $('#embedViewer').attr('src', '');
            $('#embedViewer').attr('src', '<?php echo base_url(); ?>/cetak/print_st/tipe2/' + rowData.id_st + '/A4/' + ds, rowData.id_st);
        });


        $('#kt_datatable tbody').on('click', '.detail', function() {
            var rowData = oTable.row($(this).closest("tr")).data();
            // alert(rowData.tgl_st);
            $('#detailModal').modal('show');
            $('input[name ="id_st_5"]').val(rowData.id_st);
            $('textarea[name ="header_st"]').val(rowData.header_st);
            $('textarea[name ="dasar_st"]').val(rowData.dasar_st);
            $('textarea[name ="peruntukan_st"]').val(rowData.peruntukan_st);
            $('textarea[name ="jabatan_spesimen"]').val(rowData.jabatan_spesimen);
            $('input[name ="nama_spesimen"]').val(rowData.nama_spesimen);
            $('input[name ="nip_spesimen"]').val(rowData.nip_spesimen);
            $('input[name ="nama_dipa"]').val(rowData.nama_dipa);
            $('input[name ="no_dipa"]').val(rowData.no_dipa);
            $('#tgl_dipa').datepicker('update', '').datepicker('update', rowData.tgl_dipa.split("-").reverse().join("-"));

        });

        $('#kt_datatable tbody').on('click', '.hapusTujuan', function() {
            var id = $(this).attr('id_tujuan');
            Swal.fire({
                title: "Akan menghapus tujuan ini?",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: "Baik",
                cancelButtonText: "Batal"
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url() . '/perjadin/st/hapusTujuan/'; ?>' + id,
                        data: {
                            id_tujuan: id,
                        },
                        success: function(result) {
                            var data = $.parseJSON(result);
                            jsonMessageAlert(data['result'], data['message']);
                            if (data['result'] == 'success') {
                                oTable.ajax.reload(null,false);
                            }
                        }
                    });
                }
            });
        });

        $('#kt_datatable tbody').on('click', '.hapusTertugas', function() {
            var id = $(this).attr('id_tertugas');
            Swal.fire({
                title: "Akan menghapus pegawai yang bertugas ini?",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: "Baik",
                cancelButtonText: "Batal"
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url() . '/perjadin/st/hapusTertugas/'; ?>' + id,
                        data: {
                            id_tertugas: id,
                        },
                        success: function(result) {
                            var data = $.parseJSON(result);
                            jsonMessageAlert(data['result'], data['message']);
                            if (data['result'] == 'success') {
                                oTable.ajax.reload(null,false);
                            }
                        }
                    });
                }
            });
        });
        $('#kt_datatable tbody').on('click', '.hapus', function() {

            var rowData = oTable.row($(this).closest("tr")).data();
            var id = rowData.id_st;
            Swal.fire({
                title: "Apakah anda akan menghapus data ini?",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: "Hapus!",
                cancelButtonText: "Batal"
            }).then(function(resultx) {
                if (resultx.value) {
                    Swal.fire({
                        title: "Data yang sudah dihapus tidak dapat dikembalikan lagi.",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Baik",
                        cancelButtonText: "Batal"
                    }).then(function(result) {
                        if (result.value) {
                            $.ajax({
                                type: 'post',
                                url: '<?php echo base_url() . '/perjadin/st/hapusST/'; ?>' + id,
                                data: {
                                    id_st: id,
                                },
                                success: function(result) {
                                    var data = $.parseJSON(result);
                                    jsonMessageAlert(data['result'], data['message']);
                                    if (data['result'] == 'success') {
                                        oTable.ajax.reload(null,false);
                                    }
                                }
                            });
                        }
                    });
                }
            });
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $('#myForm').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/perjadin/st/simpanST'; ?>',
                data: $('#myForm').serialize(),
                success: function(result) {
                    var data = $.parseJSON(result);
                    jsonMessageAlert(data['result'], data['message']);
                    if (data['result'] == 'success') {
                        $("#myFormModal").modal('hide');
                        oTable.ajax.reload(null,false);
                    }
                }
            });
        });
        $('#detailForm').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/perjadin/st/simpanDetail'; ?>',
                data: $('#detailForm').serialize(),
                success: function(result) {
                    var data = $.parseJSON(result);
                    jsonMessageAlert(data['result'], data['message']);
                    if (data['result'] == 'success') {
                        $("#detailModal").modal('hide');
                        oTable.ajax.reload(null,false);
                    }
                }
            });
        });
        $('#kegiatanForm').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/perjadin/st/simpanKegiatan'; ?>',
                data: $('#kegiatanForm').serialize(),
                success: function(result) {
                    var data = $.parseJSON(result);
                    jsonMessageAlert(data['result'], data['message']);
                    if (data['result'] == 'success') {
                        $("#kegiatanModal").modal('hide');
                        oTable.ajax.reload(null,false);
                    }
                }
            });
        });

        $('#tujuanForm').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/perjadin/st/simpanTujuan'; ?>',
                data: $('#tujuanForm').serialize(),
                success: function(result) {
                    var data = $.parseJSON(result);
                    jsonMessageAlert(data['result'], data['message']);
                    if (data['result'] == 'success') {
                        $("#tujuanModal").modal('hide');
                        oTable.ajax.reload(null,false);
                    }
                }
            });
        });
        $('#tertugasForm').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/perjadin/st/simpanTertugas'; ?>',
                data: $('#tertugasForm').serialize(),
                success: function(result) {
                    var data = $.parseJSON(result);
                    jsonMessageAlert(data['result'], data['message']);
                    if (data['result'] == 'success') {
                        $("#tertugasModal").modal('hide');
                        oTable.ajax.reload(null,false);
                    }
                }
            });
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $("#kegiatanModal").on('hidden.bs.modal', function() {
            $("#kegiatanForm")[0].reset();
            // oTable.ajax.reload(null,false);
            $('input[name ="id_st_2"]').val(0);
            $('textarea[name ="kegiatan_st"]').val('');

        });
        $("#myFormModal").on('hidden.bs.modal', function() {
            $("#myForm")[0].reset();
            // oTable.ajax.reload(null,false);
            $('input[name ="id_st_1"]').val(0);

        });
        $("#kegiatanModal").on('shown.bs.modal', function() {
            $('textarea[name ="kegiatan_st"]').focus();

        });
        $("#viewerModal").on('hidden.bs.modal', function() {
            $('#embedViewer').attr('src', '');
        });
    });
    // $('textarea').on('keyup keypress', function(e) {
    //     var keyCode = e.keyCode || e.which;
    //     if (keyCode === 13) {
    //         e.preventDefault();
    //         return false;
    //     }
    // });
</script>
<!--end::Page Scripts-->
<?php echo view('index/0-bottom'); ?>