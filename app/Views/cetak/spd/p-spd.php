<?php

require_once('public/TCPDF-main/tcpdf.php');
class MYPDF extends TCPDF
{
    public function Footer()
    {
        // $this->SetFont('', '', 6);

        // $style = array(
        //     'position' => '',
        //     'align' => 'C',
        //     'stretch' => false,
        //     'fitwidth' => true,
        //     'cellfitalign' => '',
        //     'fgcolor' => array(0, 0, 0),
        // );
        // $this->SetY(-4);
        // $this->SetX(1);
        // $this->write1DBarcode($this->urutan_st, 'C93', '', '', 200, 3, 0.6, $style, 'N');
    }
}

// create new PDF document
$pdf = new MYPDF('P', 'mm', $size, true, 'UTF-8', false);
$pdf->urutan_st = $tertugas['urutan_st'];

// set document information
$pdf->SetAuthor('Teddy Cahyo Munanto');
$pdf->SetTitle('SPD ' . str_pad($tertugas['urutan_st'], 3, "0", STR_PAD_LEFT) . ' ' . $tertugas['nama']);


// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetPrintHeader(false);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

$header = 'KANTOR REGIONAL VIII
BADAN KEPEGAWAIAN NEGARA';
$no_spd = str_replace('ST', 'SPD', $tertugas['no_st']);

$namapegawai = strtoupper(tulisGelar($tertugas['nama'], $tertugas['gelarDepan'], $tertugas['gelarBelakang']));
$nipBaru = strtoupper($tertugas['nipBaru']);
$pangkatAkhir = strtoupper($tertugas['pangkatAkhir']);
$golRuangAkhir = ($tertugas['golRuangAkhir']);
$jabatannama = strtoupper($tertugas['jabatannama']);

$kegiatan_st = strtoupper('DALAM RANGKA ' . trim(preg_replace('/\s\s+/', ' ', $tertugas['kegiatan_st'])));
$lokasikegiatan = strtoupper(trim(preg_replace('/\s\s+/', ' ', $tertugas['detail_tujuan'] . ', ' . $tertugas['nama_kabkota'] . ', ' . $tertugas['nama_prov'])));

$angkutan = '';
if ($tertugas['trans_darat'] && $tertugas['trans_laut'] && $tertugas['trans_udara']) {
    $angkutan = 'ANGKUTAN DARAT, KAPAL DAN PESAWAT.';
} else if ($tertugas['trans_laut'] && $tertugas['trans_udara']) {
    $angkutan = 'ANGKUTAN KAPAL DAN PESAWAT.';
} else if ($tertugas['trans_darat']  && $tertugas['trans_udara']) {
    $angkutan = 'ANGKUTAN DARAT DAN PESAWAT.';
} else if ($tertugas['trans_darat'] && $tertugas['trans_laut']) {
    $angkutan = 'ANGKUTAN DARAT DAN KAPAL.';
} else if ($tertugas['trans_darat']) {
    $angkutan = 'ANGKUTAN DARAT';
} else if ($tertugas['trans_laut']) {
    $angkutan = 'ANGKUTAN KAPAL';
} else if ($tertugas['trans_udara']) {
    $angkutan = 'ANGKUTAN PESAWAT';
}

$kota_asal = strtoupper($tertugas['kota_asal']);
$nama_kabkota = strtoupper($tertugas['nama_kabkota']);


$jumlahhari = strtoupper(floor(1 + (strtotime($tertugas['tgl_akhir']) - strtotime($tertugas['tgl_awal'])) / 86400));
$tgl_awal = strtoupper(convertDate($tertugas['tgl_awal']));
$tgl_akhir = strtoupper(convertDate($tertugas['tgl_akhir']));


$anggaran = strtoupper($tertugas['kode_mak']);
$anggaran_akun = substr(strtoupper($tertugas['kode_mak']), strrpos(strtoupper($tertugas['kode_mak']), '.') + 1);
$anggaran_instansi = strtoupper($tertugas['anggaran_instansi']);
$tingkat_spd = strtoupper($tertugas['tingkat_spd']);
$lokasi_spd =  $tertugas['lokasi_spd'];
$tgl_spd = convertDate($tertugas['tgl_spd']);
$nama_ppk = $tertugas['nama_ppk'];
$nip_ppk = $tertugas['nip_ppk'];


$ukuranfontdefault = 11;


//============================================================+
$pdf->AddPage();

//LOGO HEADER
$Y = $pdf->GetY();
$image_width = 18;
$pdf->Image('public/img/logo_garuda.png', 210 / 2 - ($image_width / 2), 7, $image_width, 0, 'PNG', '', 'R', false, 300, '', false, false, 0, false, false, false);

//HEADER
$pdf->ln(22);
$pdf->SetFont('times', 'B', $ukuranfontdefault + 2);
$text = explode(PHP_EOL, $header);
$n = 1;
foreach ($text as $p) {
    $pdf->MultiCell(181, 0, $p, 0, 'C', 0, 1, '', '', true, '', true);
}

// $pdf->SetY($Y);
// $pdf->SetFont('freesans', '', 6);
// $pdf->Cell(120);
// $pdf->Cell(0, 2, 'LAMPIRAN II', 0, 2, 'L');
// $pdf->Cell(0, 2, 'PERATURAN MENTERI KEUANGAN REPUBLIK INDONESIA', 0, 2, 'L');
// $pdf->Cell(0, 2, 'NOMOR : 113/PMK.05/2012', 0, 2, 'L');
// $pdf->Cell(0, 2, 'TENTANG', 0, 2, 'L');
// $pdf->Cell(0, 2, 'PERJALANAN DINAS JABATAN DALAM NEGERI PEJABAT', 0, 2, 'L');
// $pdf->Cell(0, 2, 'NEGARA, PEGAWAI NEGERI, DAN PEGAWAI TIDAK TETAP', 0, 2, 'L');

$Y = $pdf->GetY();
$pdf->SetY($Y);

$pdf->ln(5);
$pdf->SetFont('helvetica', 'B', $ukuranfontdefault);
$pdf->Write(0, 'SURAT PERJALANAN DINAS (SPD)', '', 0, 'C', true, 0, false, false, 0);
$pdf->SetFont('helvetica', 'B', 10);
$pdf->Cell(0, 4, 'Nomor : ' . $no_spd, 0, 1, 'C');
$pdf->ln(5);
$pdf->SetFont('arialn', '', $ukuranfontdefault);

// -----------------------------------------------------------------------------

$tbl = <<<EOD
<table cellspacing="0" cellpadding="7" border="1">
<tr>
    <td style="width:5%;text-align:center;">1</td>
    <td style="width:30%;" align="left">Pejabat Pembuat Komitmen</td>
    <td style="width:41%;" align="left">$nama_ppk</td>
    <td style="width:24%;">NIP. $nip_ppk</td>
</tr>
<tr>
    <td style="text-align:center;">2</td>
    <td style="" align="left">Nama / NIP Pegawai yang melaksanakan perjalanan dinas</td>
    <td style="" align="left">$namapegawai</td>
    <td>NIP. $nipBaru </td>
</tr>
<tr>
    <td style="text-align:center;">3</td>
    <td style="" align="left">a. Pangkat dan Golongan<br/>b. Jabatan / Instansi<br/>c. Tingkat Biaya Perjalanan Dinas</td>
    <td colspan="2">a. $pangkatAkhir / $golRuangAkhir<br/>b. $jabatannama<br/>c. $tingkat_spd</td>
</tr>
<tr>
    <td style="text-align:center;">4</td>
    <td style="" align="left">Maksud Perjalanan Dinas</td>
    <td colspan="2">$kegiatan_st</td>
</tr>
<tr>
    <td style="text-align:center;">5</td>
    <td style="" align="left">Alat angkutan yang dipergunakan</td>
    <td colspan="2">$angkutan</td>
</tr>
<tr>
    <td style="text-align:center;">6</td>
    <td style="" align="left">a. Tempat berangkat<br/>b. Tempat tujuan</td>
    <td colspan="2">a. $kota_asal<br/>b. $nama_kabkota</td>
</tr>
<tr>
    <td style="text-align:center;">7</td>
    <td style="" align="left">a. Lamanya perjalanan dinas<br/>b. Tanggal berangkat<br/>c. Tanggal harus kembali</td>
    <td colspan="2">a. $jumlahhari HARI<br/>b. $tgl_awal<br/>c. $tgl_akhir</td>
</tr>
<tr>
    <td colspan align="center" rowspan="2">8</td>
    <td style="" align="left">Pengikut : Nama</td>
    <td style="text-align:center;"></td>
    <td style="text-align:center;">Keterangan</td>
</tr>
<tr>
    <td style="" align="left">1.<br>2.<br>3.</td>
    <td style="text-align:center;"></td>
    <td style="text-align:center;"></td>
</tr>
<tr>
    <td style="text-align:center;">9</td>
    <td style="" align="left">Pembebanan Anggaran<br/>a. Instansi<br/>b. Akun</td>
    <td colspan="2">$anggaran<br/>a. $anggaran_instansi<br/>b. $anggaran_akun</td>
</tr>
<tr>
    <td colspan align="center">10</td>
    <td colspan align="left" colspan="3">Keterangan lain-lain</td>
</tr>
</table>
EOD;
$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->SetFont('helvetica', '', $ukuranfontdefault);

$pdf->Ln(0);
$pdf->Cell(110);
$pdf->Cell(25, 5, 'Ditetapkan di ', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'L');
$pdf->Cell(25, 5, $lokasi_spd, 0, 0, 'L');

$pdf->Ln(5);
$pdf->Cell(110);
$X = $pdf->getX();
$pdf->Cell(25, 5, 'Pada tanggal', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'L');
$pdf->Cell(25, 5, $tgl_spd, 0, 0, 'L');
$Y = $pdf->getY();
$pdf->line($X, $Y + 5, $X + 65, $Y + 5);



$pdf->Ln(8);
$pdf->Cell(100);
$pdf->Cell(0, 5, 'Pejabat Pembuat Komitmen', 0, 0, 'C');
$pdf->Ln(20);
$pdf->Cell(100);
$pdf->writeHTML('<b>' . $nama_ppk . '</b>',  1, 0, 0, 0, 'C');
$pdf->Cell(100);
$pdf->Cell(0, 5, 'NIP. ' . $nip_ppk, 0, 0, 'C');

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('SPD_' . date('Y', strtotime($tertugas['tgl_spd'])) . ' ' . str_pad($tertugas['urutan_st'], 3, "0", STR_PAD_LEFT) . ' ' . $tertugas['nama'] . '.pdf', 'I');
exit();

//============================================================+
// END OF FILE
//============================================================+