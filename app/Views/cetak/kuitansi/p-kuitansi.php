<?php

require_once('public/TCPDF-main/tcpdf.php');
class MYPDF extends TCPDF
{
    public function Footer()
    {
        // $this->SetFont('', '', 6);

        // $style = array(
        //     'position' => '',
        //     'align' => 'C',
        //     'stretch' => false,
        //     'fitwidth' => true,
        //     'cellfitalign' => '',
        //     'fgcolor' => array(0, 0, 0),
        // );
        // $this->SetY(-4);
        // $this->SetX(1);
        // $this->write1DBarcode($this->urutan_st, 'C93', '', '', 200, 3, 0.6, $style, 'N');
    }
}

// create new PDF document
$pdf = new MYPDF('P', 'mm', $size, true, 'UTF-8', false);
$pdf->urutan_st = $tertugas['urutan_st'];

// set document information
$pdf->SetAuthor('Teddy Cahyo Munanto');
$pdf->SetTitle('Kuitansi ' . str_pad($tertugas['urutan_st'], 3, "0", STR_PAD_LEFT) . ' ' . $tertugas['nama']);


// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(0);
$pdf->SetPrintHeader(false);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

$header = 'KANTOR REGIONAL VIII
BADAN KEPEGAWAIAN NEGARA';
$no_spd = str_replace('ST', 'SPD', $tertugas['no_st']);
$no_st = ($tertugas['no_st']);
$tgl_st = ($tertugas['tgl_st']);

$namapegawai = tulisGelar($tertugas['nama'], $tertugas['gelarDepan'], $tertugas['gelarBelakang']);
$nipBaru = $tertugas['nipBaru'];
$pangkatAkhir = $tertugas['pangkatAkhir'];
$golRuangAkhir = $tertugas['golRuangAkhir'];
$jabatannama = $tertugas['jabatannama'];

$kegiatan_st = trim(preg_replace('/\s\s+/', ' ', $tertugas['kegiatan_st']));
$lokasikegiatan = trim(preg_replace('/\s\s+/', ' ', $tertugas['detail_tujuan'] . ', ' . $tertugas['nama_kabkota'] . ', ' . $tertugas['nama_prov']));


$jumlahhari = floor(1 + (strtotime($tertugas['tgl_akhir']) - strtotime($tertugas['tgl_awal'])) / 86400);
$tgl_awal = convertDate($tertugas['tgl_awal']);
$tgl_akhir = convertDate($tertugas['tgl_akhir']);


$lokasi_spd = ($tertugas['lokasi_spd']);;
$tgl_spd = convertDate($tertugas['tgl_spd']);;
$tgl_kuitansi = convertDate($tertugas['tgl_kuitansi']);;
$nama_ppk = $tertugas['nama_ppk'];
$nip_ppk = $tertugas['nip_ppk'];
$nama_bendahara = $tertugas['nama_bendahara'];
$nip_bendahara = $tertugas['nip_bendahara'];


$biaya = '';
$n = 0;
$total_biaya = 0;
foreach ($tertugas['biaya'] as $item) {
    $n++;
    $total_biaya += $item->nominal_biaya;
    $nominal_biaya = angka_rupiah($item->nominal_biaya, null);
    $biaya .= <<<EOD
    <tr>
        <td style="text-align:center;border-left: 1px solid black;border-right: 1px solid black;">$n</td>
        <td style="text-align:left;border-left: 1px solid black;border-right: 1px solid black;">$item->rincian_biaya</td>
        <td style="text-align:right;border-left: 1px solid black;border-right: 1px solid black;">$nominal_biaya</td>
        <td style="text-align:left;border-left: 1px solid black;border-right: 1px solid black;">$item->keterangan_biaya</td>
    </tr>
    EOD;
}
$angka_total_biaya = angka_rupiah($total_biaya);
$terbilang_total_biaya = terbilang_rupiah($total_biaya);


$ukuranfontdefault = 11;
//============================================================+ KUITANSI
$pdf->AddPage();

//LOGO HEADER
$Y = $pdf->GetY();
$image_width = 18;
$pdf->Image('public/img/logo_garuda.png', 210 / 2 - ($image_width / 2), 7, $image_width, 0, 'PNG', '', 'R', false, 300, '', false, false, 0, false, false, false);

//HEADER
$pdf->ln(22);
$pdf->SetFont('times', 'B', $ukuranfontdefault + 2);
$text = explode(PHP_EOL, $header);
$n = 1;
foreach ($text as $p) {
    $pdf->MultiCell(181, 0, $p, 0, 'C', 0, 1, '', '', true, '', true);
}

// $pdf->SetY($Y);
// $pdf->SetFont('freesans', '', 6);
// $pdf->Cell(120);
// $pdf->Cell(0, 2, 'LAMPIRAN II', 0, 2, 'L');
// $pdf->Cell(0, 2, 'PERATURAN MENTERI KEUANGAN REPUBLIK INDONESIA', 0, 2, 'L');
// $pdf->Cell(0, 2, 'NOMOR : 113/PMK.05/2012', 0, 2, 'L');
// $pdf->Cell(0, 2, 'TENTANG', 0, 2, 'L');
// $pdf->Cell(0, 2, 'PERJALANAN DINAS JABATAN DALAM NEGERI PEJABAT', 0, 2, 'L');
// $pdf->Cell(0, 2, 'NEGARA, PEGAWAI NEGERI, DAN PEGAWAI TIDAK TETAP', 0, 2, 'L');

$Y = $pdf->GetY();
$pdf->SetY($Y);

$pdf->ln(5);
$pdf->SetFont('helvetica', 'B', $ukuranfontdefault);
$pdf->Write(0, 'KUITANSI', '', 0, 'C', true, 0, false, false, 0);
$pdf->ln(5);
$pdf->SetFont('', '', 9);

$pdf->Cell(30, 3, 'Sudah Terima dari ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, 'Kuasa Pengguna Anggaran Kantor Regional VIII Badan Kepegawaian Negara', 0, 1, 'L');

$pdf->Cell(30, 3, 'Uang Sebesar ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, $angka_total_biaya . ' (' . $terbilang_total_biaya . ')', 0, 1, 'L');

$pdf->Cell(30, 3, 'Guna Pembayaran ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->MultiCell(0, 3, 'Biaya melakukan perjalanan dinas dalam rangka ' . $kegiatan_st . ' bertempat di ' . $lokasikegiatan.'.', 0, 'L');
$pdf->ln(2);

$pdf->Cell(40);
$pdf->Cell(30, 3, 'Dari ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, strtoupper('Kepala Kantor Regional VIII BKN'), 0, 1, 'L');

$pdf->Cell(40);
$pdf->Cell(30, 3, 'Nomor ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, $no_st, 0, 1, 'L');

$pdf->Cell(40);
$pdf->Cell(30, 3, 'Tanggal ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, convertDate($tgl_st), 0, 1, 'L');

$pdf->ln(2);
$pdf->Cell(30, 3, 'Nama ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, "$namapegawai", 0, 1, 'L');


$pdf->Cell(30, 3, 'NIP ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, "$nipBaru", 0, 1, 'L');

$pdf->Cell(30, 3, 'Pangkat/Jabatan ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, strtoupper("$pangkatAkhir / $golRuangAkhir"), 0, 2, 'L');
$pdf->Cell(0, 3, strtoupper($jabatannama), 0, 1, 'L');


$pdf->Cell(30, 3, 'Tempat Kedudukan ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, $lokasi_spd, 0, 1, 'L');

$Y = $pdf->GetY();

$pdf->Cell(90);
$pdf->Cell(100, 3, 'Yang bepergian', 0, 1, 'C');
$pdf->Ln(12);
$pdf->Cell(90);
$pdf->MultiCell(100, 3, "<b>$namapegawai</b>",  0, 'C', 0, 1, '', '', 1, 0, 1);
$pdf->Cell(90);
$pdf->Cell(100, 3, "NIP $nipBaru", 0, 0, 'C');

$pdf->SetY($Y);
$pdf->Ln(5);
$pdf->Ln(20);
$pdf->Cell(35, 3, 'Lampiran SPD Nomor ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, $no_spd, 0, 1, 'L');

$pdf->Cell(35, 3, 'Tanggal ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, $tgl_spd, 0, 1, 'L');




$pdf->SetFont('', 'B', 11);
$pdf->ln(2);
$pdf->Write(0, 'RINCIAN PERJALANAN DINAS', '', 0, 'C', true, 0, false, false, 0);
$pdf->ln(3);
$pdf->SetFont('', '', 9);
// -----------------------------------------------------------------------------

$tbl = <<<EOD
<table cellspacing="0" cellpadding="1">
<tr>
    <th style="border: 1px solid black; width:'5%;text-align:center;"><b>No.</b></th>
    <th style="border: 1px solid black; width:'50%;" align="center"><b>Perincian Biaya</b></th>
    <th style="border: 1px solid black; width:'15%;" align="center"><b>Jumlah</b></th>
    <th style="border: 1px solid black; width:'30%;" align="center"><b>Keterangan</b></th>
</tr>
$biaya
<tr>
    <td style="border: 1px solid black;" colspan="2" align="left"><b> Jumlah</b></td>
    <td style="border: 1px solid black;" align="right"><b>$angka_total_biaya</b></td>
    <td style="border: 1px solid black;" ></td>
</tr>
<tr>
    <td style="border: 1px solid black;" colspan="4"> Terbilang : <b>$terbilang_total_biaya</b></td>
</tr>

</table>
EOD;
$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->Ln(0);
$pdf->Cell(130);
$pdf->Cell(0, 5, "$lokasi_spd, $tgl_kuitansi", 0, 1, 'C');
$pdf->Ln(2);
$Y = $pdf->getY();



$pdf->setY($Y);
$pdf->Cell(100, 3, 'Telah dibayar sejumlah', 0, 1, 'C');
$pdf->MultiCell(100, 3, "<b>$angka_total_biaya</b>",  0, 'C', 0, 1, '', '', 1, 0, 1);
$pdf->Ln(2);
$pdf->Cell(100, 3, 'Bendahara Pengeluaran,', 0, 0, 'C');
$pdf->Ln(15);
$pdf->MultiCell(100, 3, "<b>$nama_bendahara</b>",  0, 'C', 0, 1, '', '', 1, 0, 1);
$pdf->Cell(100, 3, "NIP $nip_bendahara", 0, 0, 'C');



$pdf->setY($Y);
$pdf->Cell(80);
$pdf->Cell(100, 3, 'Telah menerima jumlah uang sebesar', 0, 1, 'C');
$pdf->Cell(80);
$pdf->MultiCell(100, 3, "<b>$angka_total_biaya</b>",  0, 'C', 0, 1, '', '', 1, 0, 1);
$pdf->Ln(2);
$pdf->Cell(80);
$pdf->Cell(100, 3, 'Yang menerima,', 0, 0, 'C');
$pdf->Ln(15);
$pdf->Cell(80);
$pdf->MultiCell(100, 3, "<b>$namapegawai</b>",  0, 'C', 0, 1, '', '', 1, 0, 1);
$pdf->Cell(80);
$pdf->Cell(100, 3, "NIP $nipBaru", 0, 0, 'C');

$Y = $pdf->getY();

$pdf->Ln(2);
$pdf->Line(5, $Y + 5, 210 - 5, $Y + 5);
$pdf->Ln(5);


$pdf->SetFont('', 'B', 11);
$pdf->Write(0, 'PERHITUNGAN SPD RAMPUNG', '', 0, 'C', true, 0, false, false, 0);
$pdf->SetFont('', '', 9);

$pdf->Ln(5);

$Yn = $pdf->getY();
$pdf->Cell(40, 3, 'Ditetapkan sejumlah ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(4, 3, 'Rp', 0, 0, 'L');
$pdf->Cell(25, 3, angka_rupiah($total_biaya, null), 0, 1, 'R');

$pdf->Cell(40, 3, 'Yang telah dibayar semula ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(4, 3, 'Rp', 0, 0, 'L');
$pdf->Cell(25, 3, '-', 0, 1, 'R');

$Y = $pdf->getY();
$pdf->Line(60, $Y, 87, $Y);

$pdf->SetFont('', 'B', 9);
$pdf->Cell(40, 3, 'Sisa kurang/lebih ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(4, 3, 'Rp', 0, 0, 'L');
$pdf->Cell(25, 3, angka_rupiah($total_biaya, null), 0, 1, 'R');
$pdf->SetFont('', '', 9);


$pdf->setY($Yn);
$pdf->Cell(100);
$pdf->Cell(0, 3, "Pejabat Pembuat Komitmen", 0, 0, 'C');
$pdf->Cell(100);
$pdf->Cell(0, 3, 'Kantor Regional VIII BKN', 0, 0, 'C');
$pdf->Ln(15);
$pdf->Cell(100);
$pdf->writeHTML('<b>' . $nama_ppk . '</b>',  1, 0, 0, 0, 'C');
$pdf->Cell(100);
$pdf->writeHTML('NIP. ' . $nip_ppk,  0, 0, 0, 0, 'C');
// -----------------------------------------------------------------------------


// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Kuitansi_' . date('Y', strtotime($tertugas['tgl_spd'])) . ' ' . str_pad($tertugas['urutan_st'], 3, "0", STR_PAD_LEFT) . ' ' . $tertugas['nama'] . '.pdf', 'I');
exit();

//============================================================+
// END OF FILE
//============================================================+