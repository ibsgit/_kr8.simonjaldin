<!--begin::Card-->
<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="fas fa-users-cog text-primary"></i>
            </span>
            <h3 class="card-label"><?php echo $page; ?></h3>
        </div>

    </div>
    <form class="form" id="myForm">
        <div class="card-body">

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group row">
                        <label class="col-lg-5 col-form-label">Username</label>
                        <div class="col-lg-7">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-user-tag text-success"></i>
                                    </span>
                                </div>
                                <?php echo form_input($username); ?>
                                <?php echo form_input($user_id); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-5 col-form-label">Nama Lengkap</label>
                        <div class="col-lg-7">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-id-card text-success"></i>
                                    </span>
                                </div>
                                <?php echo form_input($first_name); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-5 col-form-label">Email</label>
                        <div class="col-lg-7">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-envelope text-success"></i>
                                    </span>
                                </div>
                                <?php echo form_input($email); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-5 col-form-label">No. HP</label>
                        <div class="col-lg-7">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-phone text-success"></i>
                                    </span>
                                </div>
                                <?php echo form_input($phone); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group row">
                        <label class="col-lg-5 col-form-label">Password Lama</label>
                        <div class="col-lg-7">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-key text-success"></i>
                                    </span>
                                </div>
                                <?php echo form_input($old_password); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-5 col-form-label">Password Baru</label>
                        <div class="col-lg-7">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-key text-success"></i>
                                    </span>
                                </div>
                                <?php echo form_input($new_password); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-5 col-form-label">Konfirmasi Password Baru</label>
                        <div class="col-lg-7">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-key text-success"></i>
                                    </span>
                                </div>
                                <?php echo form_input($new_password_confirm); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-12 text-lg-right">
                    <button type="submit" class="btn btn-primary font-weight-bold" id="submit">Simpan</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!--end::Card-->
<?php require('app/Views/index/i-js-global.php'); ?>
<script>
    $('#myForm').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '<?php echo base_url() . '/auth/profil/'; ?>' + $('input[name ="user_id"]').val(),
            data: $('#myForm').serialize(),
            success: function(result) {
                var data = $.parseJSON(result);
                jsonMessageAlert(data['result'], data['message']);
                if (data['result'] == 'success') {
                    $('input[name ="old"]').val('');
                    $('input[name ="new"]').val('');
                    $('input[name ="new_confirm"]').val('');
                    $('input[name ="username"]').focus();
                }
            }
        });


    });
</script>