<?php

namespace App\Controllers;

use App\Models\model_st;
use App\Models\model_pegawai;
use App\Models\model_log;

class Dashboard extends BaseController
{
    public function __construct()
    {
        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to(base_url());
        }
        $this->data['userdata'] = $this->ionAuth->user()->row();
    }

    public function index()
    {
        //JUMLAH PEGAWAI
        $model_st = new model_st();
        $count_st = $model_st
            ->select('count(*) as numRows')
            ->where('YEAR(tgl_st)', date('Y'))
            ->get()->getRow()->numRows;
        $this->data['count_st'] = $count_st;

        //JUMLAH SPD
        $model_st = new model_st();
        $count_spd = $model_st
            ->select('count(*) as numRows')
            ->join('data_spd', 'data_spd.id_st = data_st.id_st')
            ->where('YEAR(tgl_st)', date('Y'))
            ->get()->getRow()->numRows;
        $this->data['count_spd'] = $count_spd;

        //JUMLAH TRIP
        $model_st = new model_st();
        $count_trip = $model_st
            ->select('count(*) as numRows')
            ->join('data_tertugas', 'data_tertugas.id_st = data_st.id_st')
            ->where('YEAR(tgl_st)', date('Y'))
            ->get()->getRow()->numRows;
        $this->data['count_trip'] = $count_trip;

        //JUMLAH PEGAWAI
        $model_pegawai = new model_pegawai();
        $count_pegawai = $model_pegawai
            ->select('count(*) as numRows')
            ->get()->getRow()->numRows;
        $this->data['count_pegawai'] = $count_pegawai;

        //DATA LOG
        $model_log = new model_log();
        $data_log = $model_log
            ->select('*,DATE(log_datetime) as tanggal, TIME(log_datetime) as jam')
            ->join('auth_users', 'log.log_user = auth_users.id')
            ->asObject()
            ->orderBy('log_datetime', 'DESC')
            ->limit(5)
            ->find();
        $this->data['data_log'] = $data_log;

        //DATA DINAS TERAKHIR
        $db = \Config\Database::connect();

        $data_dinas_terakhir = $db
            ->query("
            SELECT tgl_awal,tgl_akhir,nama_kabkota, kegiatan_st,total,GROUP_CONCAT(nama SEPARATOR '<br/>') AS tertugas,COUNT(*) - COUNT(total) AS kuitansi_null,COUNT(*) - COUNT(id_spd) AS spd_null  FROM data_st a
LEFT JOIN data_tujuan b ON a.id_st=b.id_st
LEFT JOIN tabel_kabkota c ON b.id_kabkota=c.id_kabkota
LEFT JOIN data_tertugas d ON b.id_tujuan=d.id_tujuan
LEFT JOIN tabel_pegawai e ON d.nip_pegawai=e.nipBaru
LEFT JOIN data_spd f ON a.id_st=f.id_st
LEFT JOIN (SELECT id_tertugas,SUM(nominal_biaya) AS total FROM data_biaya GROUP BY id_tertugas) g ON g.id_tertugas=d.id_tertugas
GROUP BY b.id_tujuan
ORDER BY tgl_awal DESC
LIMIT 20
")
            ->getResult();
        $this->data['data_dinas_terakhir'] = $data_dinas_terakhir;


        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to(base_url());
        }
        $this->data['page'] = 'Dashboard';
        echo view('index/0-top', $this->data);
        echo view('dashboard/dash_index', $this->data);
        echo view('index/0-bottom');
    }

    //--------------------------------------------------------------------

}
