<?php

namespace App\Controllers\api;

use App\Controllers\BaseController;
use App\Models\model_tujuan;
use App\Models\model_tertugas;
use App\Models\model_biaya;
use App\Models\model_spd;


class Datatables extends BaseController
{
    public function __construct()
    {
        require_once(APPPATH . 'ThirdParty/DataTables/ssp.class.php');
        $this->db = db_connect();
    }

    public function user()
    {
        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        $table = <<<EOT
        (
            SELECT * from auth_users
        ) temp
        EOT;
        $primaryKey = 'id';
        $sql_details = array(
            "host" => $this->db->hostname,
            "user" => $this->db->username,
            "pass" => $this->db->password,
            "db" => $this->db->database,
        );
        $columns = array(
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'username', 'dt' => 'username'),
            array('db' => 'email',  'dt' => 'email'),
            array('db' => 'phone',  'dt' => 'phone'),
            array('db' => 'first_name',   'dt' => 'first_name'),
            array('db' => 'active',     'dt' => 'active'),
            array('db' => 'last_login', 'dt' => 'last_login', 'formatter' => function ($d, $row) {
                return date("Y-m-d H:i:s", $d);
            }),
            array('db' => 'id', 'dt' => 'groups', 'formatter' => function ($d, $row) {
                return $this->ionAuth->getUsersGroups($d)->getResult();
            }),
        );
        echo json_encode(
            \SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }

    //--------------------------------------------------------------------

    public function st()
    {

        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        $table = <<<EOT
        (
            SELECT a.*,GROUP_CONCAT(c.nama SEPARATOR ' ') AS petugas FROM data_st a
            LEFT JOIN data_tertugas b ON a.id_st=b.id_st
            LEFT JOIN tabel_pegawai c ON b.nip_pegawai=c.nipBaru
            WHERE deleted IS NULL
            GROUP BY a.id_st
        ) temp
        EOT;
        $primaryKey = 'id_st';
        $sql_details = array(
            "host" => $this->db->hostname,
            "user" => $this->db->username,
            "pass" => $this->db->password,
            "db" => $this->db->database,
        );
        $columns = array(
            array('db' => 'id_st', 'dt' => 'id_st'),
            array('db' => 'no_st', 'dt' => 'no_st'),
            array('db' => 'tgl_st',  'dt' => 'tgl_st'),
            array('db' => 'urutan_st',  'dt' => 'urutan_st'),
            array('db' => 'kegiatan_st',  'dt' => 'kegiatan_st'),
            array('db' => 'dasar_st',  'dt' => 'dasar_st'),
            array('db' => 'peruntukan_st',  'dt' => 'peruntukan_st'),
            array('db' => 'jabatan_spesimen',  'dt' => 'jabatan_spesimen'),
            array('db' => 'nip_spesimen',  'dt' => 'nip_spesimen'),
            array('db' => 'nama_spesimen',  'dt' => 'nama_spesimen'),
            array('db' => 'kota_st',  'dt' => 'kota_st'),
            array('db' => 'nama_dipa',  'dt' => 'nama_dipa'),
            array('db' => 'no_dipa',  'dt' => 'no_dipa'),
            array('db' => 'tgl_dipa',  'dt' => 'tgl_dipa'),
            array('db' => 'header_st',  'dt' => 'header_st'),
            array('db' => 'petugas',  'dt' => 'petugas'),
            array('db' => 'id_st', 'dt' => 'tujuan_st', 'formatter' => function ($d, $row) {
                $model_tujuan = new model_tujuan();
                $result = $model_tujuan
                    ->select('id_tujuan,detail_tujuan,tgl_awal,tgl_akhir,tabel_kabkota.*')
                    ->join('tabel_kabkota', 'tabel_kabkota.id_kabkota = data_tujuan.id_kabkota')
                    ->where('id_st', $d)
                    ->findAll();
                return ($result);
            }),

            array('db' => 'id_st', 'dt' => 'tertugas_st', 'formatter' => function ($d, $row) {
                $model_tertugas = new model_tertugas();
                $result = $model_tertugas
                    ->select('data_tertugas.*,tabel_pegawai.nama,tabel_kabkota.nama_kabkota')
                    ->join('tabel_pegawai', 'tabel_pegawai.nipBaru = data_tertugas.nip_pegawai')
                    ->join('data_tujuan', 'data_tujuan.id_tujuan = data_tertugas.id_tujuan')
                    ->join('tabel_kabkota', 'tabel_kabkota.id_kabkota = data_tujuan.id_kabkota')
                    ->where('data_tertugas.id_st', $d)
                    ->orderBy('data_tertugas.id_tertugas', 'asc')
                    ->findAll();
                return ($result);
            }),
            array('db' => 'tgl_st', 'dt' => 'tgl_st', 'formatter' => function ($d, $row) {
                return date_format(date_create($d), "d-m-Y");
            }),
            array('db' => 'spesimen_st',  'dt' => 'spesimen_st'),
        );
        echo json_encode(
            \SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }
    //--------------------------------------------------------------------

    public function spd()
    {

        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        $table = <<<EOT
        (
            SELECT a.*,GROUP_CONCAT(c.nama SEPARATOR ' ') AS petugas FROM data_st a
            LEFT JOIN data_tertugas b ON a.id_st=b.id_st
            LEFT JOIN tabel_pegawai c ON b.nip_pegawai=c.nipBaru
            WHERE deleted IS NULL AND b.id_st IS NOT NULL
            GROUP BY a.id_st
        ) temp
        EOT;
        $primaryKey = 'id_st';
        $sql_details = array(
            "host" => $this->db->hostname,
            "user" => $this->db->username,
            "pass" => $this->db->password,
            "db" => $this->db->database,
        );
        $columns = array(
            array('db' => 'id_st', 'dt' => 'id_st'),
            array('db' => 'no_st', 'dt' => 'no_st'),
            array('db' => 'tgl_st',  'dt' => 'tgl_st'),
            array('db' => 'urutan_st',  'dt' => 'urutan_st'),
            array('db' => 'kegiatan_st',  'dt' => 'kegiatan_st'),
            // array('db' => 'dasar_st',  'dt' => 'dasar_st'),
            // array('db' => 'peruntukan_st',  'dt' => 'peruntukan_st'),
            array('db' => 'jabatan_spesimen',  'dt' => 'jabatan_spesimen'),
            array('db' => 'nip_spesimen',  'dt' => 'nip_spesimen'),
            array('db' => 'nama_spesimen',  'dt' => 'nama_spesimen'),
            array('db' => 'kota_st',  'dt' => 'kota_st'),
            array('db' => 'nama_dipa',  'dt' => 'nama_dipa'),
            array('db' => 'no_dipa',  'dt' => 'no_dipa'),
            array('db' => 'tgl_dipa',  'dt' => 'tgl_dipa'),
            // array('db' => 'header_st',  'dt' => 'header_st'),
            array('db' => 'petugas',  'dt' => 'petugas'),
            array('db' => 'id_st', 'dt' => 'data_spd', 'formatter' => function ($d, $row) {
                $model_spd = new model_spd();
                $spd = $model_spd
                    ->select('*')
                    ->where('id_st', $d)
                    ->join('tabel_mak', 'tabel_mak.id_mak = data_spd.id_mak')
                    ->findAll();
                // print_r($spd);
                if (count($spd) > 0) {
                    return ($spd[0]);
                } else {
                    return null;
                }
            }),
            array('db' => 'id_st', 'dt' => 'tertugas_st', 'formatter' => function ($d, $row) {
                $model_tertugas = new model_tertugas();
                $model_biaya = new model_biaya();

                $biaya = $model_biaya
                    ->where('id_st', $d)
                    ->orderBy('id_tertugas', 'asc')
                    ->orderBy('urutan', 'asc')
                    ->findAll();


                $query_sbm = "
                    (SELECT t.id_prov,
                    MAX(CASE WHEN t.kode_sbm = '1' THEN t.sbm ELSE NULL END) AS harian,
                    MAX(CASE WHEN t.kode_sbm = '10' THEN t.sbm ELSE NULL END) AS hotel1,
                    MAX(CASE WHEN t.kode_sbm = '20' THEN t.sbm ELSE NULL END) AS hotel2,
                    MAX(CASE WHEN t.kode_sbm = '30' THEN t.sbm ELSE NULL END) AS hotel3,
                    MAX(CASE WHEN t.kode_sbm = '40' THEN t.sbm ELSE NULL END) AS hotel4
                    FROM `tabel_sbm` t
                    GROUP BY t.id_prov) new_sbm
                    ";
                $tertugas = $model_tertugas
                    ->select('data_tertugas.*,tabel_pegawai.nama,tabel_kabkota.nama_kabkota,data_tujuan.tgl_awal,data_tujuan.tgl_akhir,new_sbm.*,tabel_prov.nama_prov')
                    ->join('tabel_pegawai', 'tabel_pegawai.nipBaru = data_tertugas.nip_pegawai')
                    ->join('data_tujuan', 'data_tujuan.id_tujuan = data_tertugas.id_tujuan')
                    ->join('tabel_kabkota', 'tabel_kabkota.id_kabkota = data_tujuan.id_kabkota')
                    ->join('tabel_prov', 'tabel_prov.id_prov = tabel_kabkota.id_prov')
                    ->join($query_sbm, 'new_sbm.id_prov = tabel_prov.id_prov')
                    ->where('data_tertugas.id_st', $d)
                    ->orderBy('data_tertugas.id_tertugas', 'asc')
                    ->findAll();
                $temp = [];
                foreach ($tertugas as $t) {
                    foreach ($biaya as $key => $val) {
                        if ($val['id_tertugas'] === $t['id_tertugas']) {
                            $t['biaya'][] = $val;
                        }
                    }
                    $temp[$t['id_tertugas']] = $t;
                }
                return ($temp);
            }),
            array('db' => 'tgl_st', 'dt' => 'tgl_st', 'formatter' => function ($d, $row) {
                return date_format(date_create($d), "d-m-Y");
            }),
            array('db' => 'spesimen_st',  'dt' => 'spesimen_st'),
        );
        echo json_encode(
            \SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }
    //--------------------------------------------------------------------

    public function ref_pegawai()
    {

        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        $table = <<<EOT
        (
            SELECT * from tabel_pegawai
        ) temp
        EOT;
        $primaryKey = 'id';
        $sql_details = array(
            "host" => $this->db->hostname,
            "user" => $this->db->username,
            "pass" => $this->db->password,
            "db" => $this->db->database,
        );
        $columns = array(
            array('db' => 'id', 'dt' => 'id'),
            array('db' => 'nipBaru', 'dt' => 'nipBaru'),
            array('db' => 'nama',  'dt' => 'nama'),
            array('db' => 'gelarDepan',  'dt' => 'gelarDepan'),
            array('db' => 'gelarBelakang',  'dt' => 'gelarBelakang'),
            array('db' => 'email',  'dt' => 'email'),
            array('db' => 'noHp',  'dt' => 'noHp'),
        );
        echo json_encode(
            \SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );
    }
//--------------------------------------------------------------------

public function ref_mak()
{

    $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
    $table = <<<EOT
    (
        SELECT * from tabel_mak ORDER BY tahun_mak DESC, id_mak DESC
    ) temp
    EOT;
    $primaryKey = 'id_mak';
    $sql_details = array(
        "host" => $this->db->hostname,
        "user" => $this->db->username,
        "pass" => $this->db->password,
        "db" => $this->db->database,
    );
    $columns = array(
        array('db' => 'id_mak', 'dt' => 'id_mak'),
        array('db' => 'kode_mak', 'dt' => 'kode_mak'),
        array('db' => 'detail_mak',  'dt' => 'detail_mak'),
        array('db' => 'tahun_mak',  'dt' => 'tahun_mak'),
        array('db' => 'aktif',  'dt' => 'aktif'),
    );
    echo json_encode(
        \SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
}

    //--------------------------------------------------------------------


}
