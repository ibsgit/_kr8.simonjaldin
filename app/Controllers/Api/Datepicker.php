<?php

namespace App\Controllers\api;

use App\Controllers\BaseController;
use App\Models\model_tujuan;
use App\Models\model_tertugas;

class Datepicker extends BaseController
{
    public function __construct()
    {
        $this->db = db_connect();
        helper(['main', 'log']);
    }

    function tertugas_dinas($nip = null, $id_tujuan = null)
    {
        $model_tertugas = new model_tertugas();
        $data_tugas = $model_tertugas
            ->asObject()
            ->select('tgl_awal,tgl_akhir')
            ->join('data_tujuan', 'data_tujuan.id_tujuan = data_tertugas.id_tujuan')
            ->join('tabel_kabkota', 'tabel_kabkota.id_kabkota = data_tujuan.id_kabkota')
            ->where('nip_pegawai', $nip)
            ->where('nip_pegawai', $nip)
            ->findAll();
        $result = [];
        foreach ($data_tugas as $tgl) {
            $result = array_merge($result, getDatesFromRange($tgl->tgl_awal, $tgl->tgl_akhir));
        }
        $this->response->setContentType('Content-Type: application/json');

        echo json_encode($result);
    }

    function tujuan_dinas($id_tujuan = null)
    {
        $model_tujuan = new model_tujuan();
        $data_tugas = $model_tujuan
            ->asObject()
            ->select('tgl_awal,tgl_akhir')
            ->where('id_tujuan', $id_tujuan)
            ->findAll();
        $result = [];
        foreach ($data_tugas as $tgl) {
            $result = array_merge($result, getDatesFromRange($tgl->tgl_awal, $tgl->tgl_akhir));
        }
        $this->response->setContentType('Content-Type: application/json');
        echo json_encode($result);
    }
}
