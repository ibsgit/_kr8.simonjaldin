<?php

namespace App\Controllers\api;

use App\Controllers\BaseController;
use App\Models\model_referensi_spd;

class Referensi extends BaseController
{
    public function __construct()
    {
        $this->db = db_connect();
        helper(['main', 'log']);
    }

    function spd()
    {
        $model_referensi_spd = new model_referensi_spd();
        $result = $model_referensi_spd
            ->asObject()
            ->find(1);
        $this->response->setContentType('Content-Type: application/json');

        echo json_encode($result);
    }

}
