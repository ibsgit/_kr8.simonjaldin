<?php

namespace App\Controllers\referensi;

use App\Controllers\BaseController;
use App\Models\model_pegawai;

class Pegawai extends BaseController
{
    public function __construct()
    {
        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        $this->validation = \Config\Services::validation();
        $this->data['userdata'] = $this->ionAuth->user()->row();
        helper(['form', 'url', 'main', 'log']);

        if (@$_SESSION['tokenizer'] == null)
            $this->getToken();
    }

    public function index()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to(base_url('auth/signin'));
        }
        $this->data['page']     = 'Referensi Pegawai';
        echo view('index/0-top', $this->data);
        echo view('referensi/vr-pegawai', $this->data);
        echo view('index/0-bottom');
    }

    function getToken()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, base_url('api/sapk/tokenizer'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        // $this->response->setContentType('Content-Type: application/json');
        $resultArray = json_decode($result);
        $_SESSION['tokenizer'] = $resultArray->access_token;
    }

    function getDataUtama()
    {
        $allNIP = array_unique(explode(",", rtrim(trim($this->request->getPost('nips')), ',')));
        // print_r($allNIP);
        $countSuccess = 0;
        $countError = 0;
        $nipError = '';
        $spacer = '';
        foreach ($allNIP as $nip) {
            if ($this->getDatapegawai($nip))
                $countSuccess++;
            else {
                $spacer = '';
                $countError++;
                $nipError .= $spacer . $nip;
                $spacer = '|';
            }
        }
        if ($countSuccess > 0) {
            json_message('success', $countSuccess . ' data berhasil, ' . $countError . ' data gagal ' . $nipError);
        } else {
            json_message('error', 'Semua data gagal diupdate');
        }
    }

    function getDatapegawai($nip = null, $servis = 'data-utama')
    {
        $path = '/api/pns/' . $servis . '/' . $nip;
        $token = $_SESSION['tokenizer'];
        $urlreq = 'https://wsrv.bkn.go.id';
        if (!empty($token)) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $urlreq  . $path,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/x-www-form-urlencoded",
                    "Origin: http://localhost:20000",
                    "Authorization: Bearer " . $token
                ),
            ));
            $result = curl_exec($curl);
            if (curl_errno($curl)) {
                $error_msg = curl_error($curl);
            }
            curl_close($curl);


            if (isset($error_msg)) {
                // json_message('error', $error_msg);
                return false;
            } else {
                $data = (json_decode($result));
                if ($servis == 'data-utama') {
                    $newdata = json_decode($data->data, true);

                    $model_pegawai = new model_pegawai();
                    $newdata['tglLahir'] = reverseDate($newdata['tglLahir']);
                    $newdata['tahunLulus'] = reverseDate($newdata['tahunLulus']);
                    $newdata['tmtPns'] = reverseDate($newdata['tmtPns']);
                    $newdata['tglSkPns'] = reverseDate($newdata['tglSkPns']);
                    $newdata['tmtCpns'] = reverseDate($newdata['tmtCpns']);
                    $newdata['tglSkCpns'] = reverseDate($newdata['tglSkCpns']);
                    $newdata['tmtJabatan'] = reverseDate($newdata['tmtJabatan']);
                    $newdata['tmtGolAkhir'] = reverseDate($newdata['tmtGolAkhir']);
                    $newdata['tmtEselon'] = reverseDate($newdata['tmtEselon']);
                    $newdata['tglSuratKeteranganDokter'] = reverseDate($newdata['tglSuratKeteranganDokter']);
                    $newdata['tglSuratKeteranganBebasNarkoba'] = reverseDate($newdata['tglSuratKeteranganBebasNarkoba']);
                    $newdata['tglSkck'] = reverseDate($newdata['tglSkck']);
                    $newdata['tglMeninggal'] = reverseDate($newdata['tglMeninggal']);
                    $newdata['tglNpwp'] = reverseDate($newdata['tglNpwp']);
                    $newdata['tglSpmt'] = reverseDate($newdata['tglSpmt']);
                    $newdata['tglSttpl'] = reverseDate($newdata['tglSttpl']);
                    $newdata['jabatanNama'] = str_replace('Kantor Regional VIII BKN Banjarmasin','',$newdata['jabatanNama']);
                    $newdata['jabatanStrukturalNama'] = str_replace('Kantor Regional VIII BKN Banjarmasin','',$newdata['jabatanStrukturalNama']);
                    if (($model_pegawai->select('id')->find($newdata['id'])) == null) {
                        $model_pegawai->protect(false)->insert($newdata);
                    } else {
                        $model_pegawai->protect(false)->update($newdata['id'], $newdata);
                    }
                    $model_pegawai->protect(true);
                } else {
                    $newdata = ($data->data); //data selain data-utama
                }
                // json_message('success', 'Data berhasil diperbaharui');
                return true;
            }
        }
    }
    function coba()
    {
        $model_pegawai = new model_pegawai();
        if (($model_pegawai->find('0E2621E17362440FE050640A15023D38')) == null) {
            echo 'oke';
        } else {
            echo 'nope';
        }
    }
}
