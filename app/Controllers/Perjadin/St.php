<?php

namespace App\Controllers\perjadin;

use App\Controllers\BaseController;
use App\Models\model_st;
use App\Models\model_tujuan;
use App\Models\model_kabkota;
use App\Models\model_tertugas;
use App\Models\model_pegawai;
use App\Models\model_referensi_st;

class St extends BaseController
{
    public function __construct()
    {
        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        $this->validation = \Config\Services::validation();
        $this->data['userdata'] = $this->ionAuth->user()->row();
        helper(['form', 'url', 'main', 'log']);
    }


    public function index()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to(base_url('auth/signin'));
        }

        $model_kabkota = new model_kabkota();
        $kabkota = $model_kabkota
            ->asObject()
            ->select('tabel_prov.nama_prov,tabel_kabkota.*')
            ->join('tabel_prov', 'tabel_prov.id_prov = tabel_kabkota.id_prov')
            ->findAll();
        $this->data['kabkota']     = $kabkota;

        $model_pegawai = new model_pegawai();
        $pegawai = $model_pegawai
            ->asObject()
            ->select('nipBaru, nama')
            ->findAll();
        $this->data['pegawai'] = $pegawai;

        $this->data['page']     = 'Surat Tugas';
        echo view('perjadin/v-st', $this->data);
    }
    public function simpanST()
    {
        $validate = $this->validate([
            'tgl_st' => 'required',
        ]);
        if ($validate) {
            $model_st = new model_st();
            $model_referensi_st = new model_referensi_st();
            $no_urut = $this->request->getPost('urutan_st');
            $tgl_st = date_format(date_create($this->request->getPost('tgl_st')), "Y-m-d");

            if ($no_urut == null) {
                $no_urut = ($model_st->getLastNoSt()) + 1;
            }
            if ($this->request->getPost('id_st_1') != 0) {
                $datasimpan['id_st'] = $this->request->getPost('id_st_1');
            } else {
                $datasimpan = $model_referensi_st->find(1);
                unset($datasimpan['id_st']);
            }
            $datasimpan['no_st'] = buatNomorSurat($no_urut, $tgl_st);
            $datasimpan['urutan_st'] = $no_urut;
            $datasimpan['tgl_st'] = $tgl_st;
            $datasimpan['creator'] = $this->data['userdata']->id;
            // print_r($datasimpan);

            if ($model_st->protect(false)->save($datasimpan)) {
                save_log(NULL, 'st_simpan', $model_st->getLastQuery()->getQuery());
                json_message('success', 'Data berhasil disimpan');
            } else {
                json_message('error', 'Data gagal disimpan');
            }
        } else {
            json_message('error', $this->validator);
        }
    }

    public function simpanDetail()
    {
        $model_st = new model_st();
        $datasimpan = $this->request->getPost();
        $datasimpan['id_st'] = $datasimpan['id_st_5'];
        $datasimpan['tgl_dipa'] = date_format(date_create($datasimpan['tgl_dipa']), "Y-m-d");
        $datasimpan['dasar_st'] = trim($datasimpan['dasar_st']);
        unset($datasimpan['id_st_5']);
        // print_r($datasimpan);
        if ($model_st->protect(false)->save($datasimpan)) {
            save_log(NULL, 'detail_st_simpan', $model_st->getLastQuery()->getQuery());
            json_message('success', 'Data berhasil disimpan');
        } else {
            json_message('error', 'Data gagal disimpan');
        }
        $model_st->protect(true);
    }
    public function hapusST($id_st)
    {
        $model_st = new model_st();
        if ($id_st == $this->request->getPost('id_st')) {
            if ($model_st->delete($this->request->getPost('id_st'), false)) {
                save_log(NULL, 'st_hapus', $model_st->getLastQuery()->getQuery());
                json_message('success', 'Data berhasil dihapus');
            } else {
                json_message('error', 'Data gagal dihapus');
            }
        }
    }
    //--------------------------------------------------------------------
    public function simpanKegiatan()
    {
        $validate = $this->validate([
            'kegiatan_st' => 'required',
            'id_st_2' => 'required',
        ]);
        if ($validate) {
            $model_st = new model_st();
            if ($this->request->getPost('id_st_2') != 0) {
                $datasimpan['id_st'] = $this->request->getPost('id_st_2');
            }
            $datasimpan['kegiatan_st'] = $this->request->getPost('kegiatan_st');
            // print_r($datasimpan);
            if ($model_st->save($datasimpan)) {
                save_log(NULL, 'kegiatan_simpan', $model_st->getLastQuery()->getQuery());
                json_message('success', 'Data berhasil disimpan');
            } else {
                json_message('error', 'Data gagal disimpan');
            }
            // echo $model_st->getLastQuery()->getQuery();
        } else {
            json_message('error', $this->validator);
        }
    }
    //--------------------------------------------------------------------
    public function simpanTujuan()
    {
        $validate = $this->validate([
            'detail_tujuan' => 'required|trim',
            'id_kabkota' => 'required',
            'tgl_awal' => 'required',
            'tgl_akhir' => 'required',
            'id_st_3' => 'required',
        ]);
        if ($validate) {
            $model_tujuan = new model_tujuan();
            if ($this->request->getPost('id_tujuan') != 0) {
                $datasimpan['id_tujuan'] = $this->request->getPost('id_tujuan');
            }
            $datasimpan['detail_tujuan'] = $this->request->getPost('detail_tujuan');
            $datasimpan['id_kabkota'] = $this->request->getPost('id_kabkota');
            $datasimpan['tgl_awal'] = date_format(date_create($this->request->getPost('tgl_awal')), "Y-m-d");
            $datasimpan['tgl_akhir'] = date_format(date_create($this->request->getPost('tgl_akhir')), "Y-m-d");
            $datasimpan['id_st'] = $this->request->getPost('id_st_3');

            // print_r($datasimpan);
            if ($model_tujuan->save($datasimpan)) {
                save_log(NULL, 'tujuan_simpan', $model_tujuan->getLastQuery()->getQuery());
                json_message('success', 'Data berhasil disimpan');
            } else {
                json_message('error', 'Data gagal disimpan');
            }
            // echo $model_st->getLastQuery()->getQuery();
        } else {
            json_message('error', $this->validator);
        }
    }


    public function hapusTujuan($id_tujuan)
    {
        $model_tujuan = new model_tujuan();
        if ($id_tujuan == $this->request->getPost('id_tujuan')) {
            if ($model_tujuan->delete($this->request->getPost('id_tujuan'), false)) {
                save_log(NULL, 'tujuan_hapus', $model_tujuan->getLastQuery()->getQuery());
                json_message('success', 'Data berhasil dihapus');
            } else {
                json_message('error', 'Data gagal dihapus');
            }
        }
    }
    //--------------------------------------------------------------------
    public function simpanTertugas()
    {
        $validate = $this->validate([
            'id_tujuan' => 'required',
            'nip_pegawai' => 'required',
            'id_st_4' => 'required',
        ]);
        if ($validate) {
            $model_tertugas = new model_tertugas();
            if ($this->request->getPost('id_tertugas') != 0) {
                $datasimpan['id_tertugas'] = $this->request->getPost('id_tertugas');
            }
            $datasimpan['id_tujuan'] = $this->request->getPost('id_tujuan');
            $datasimpan['nip_pegawai'] = $this->request->getPost('nip_pegawai');
            $datasimpan['id_st'] = $this->request->getPost('id_st_4');

            // print_r($datasimpan);
            if ($model_tertugas->save($datasimpan)) {
                save_log(NULL, 'tertugas_simpan', $model_tertugas->getLastQuery()->getQuery());
                json_message('success', 'Data berhasil disimpan');
            } else {
                json_message('error', 'Data gagal disimpan');
            }
            // echo $model_st->getLastQuery()->getQuery();
        } else {
            json_message('error', $this->validator);
        }
    }


    public function hapusTertugas($id_tertugas)
    {
        $model_tertugas = new model_tertugas();
        if ($id_tertugas == $this->request->getPost('id_tertugas')) {
            if ($model_tertugas->delete($this->request->getPost('id_tertugas'), false)) {
                save_log(NULL, 'tertugas_hapus', $model_tertugas->getLastQuery()->getQuery());
                json_message('success', 'Data berhasil dihapus');
            } else {
                json_message('error', 'Data gagal dihapus');
            }
        }
    }
}
