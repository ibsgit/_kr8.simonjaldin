<?php

namespace App\Controllers\cetak;

use App\Controllers\BaseController;
use App\Models\model_st;
use App\Models\model_tujuan;
use App\Models\model_kabkota;
use App\Models\model_tertugas;
use App\Models\model_pegawai;
use App\Models\model_referensi_st;

class Print_spd extends BaseController
{
    public function __construct()
    {
        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        $this->validation = \Config\Services::validation();
        $this->data['userdata'] = $this->ionAuth->user()->row();
        helper(['form', 'url', 'main', 'log']);
    }

    public function index($id_tertugas, $size = 'A4')
    {
        $model_st = new model_st();
        $model_tertugas = new model_tertugas();
        $model_tujuan = new model_tujuan();

        $data_tertugas = $model_tertugas
            ->select('data_tertugas.kota_asal,d.nama,d.gelarDepan,d.gelarBelakang,d.nipBaru,d.golRuangAkhir,d.pangkatAkhir,d.jabatannama,c.kegiatan_st,c.no_st,c.urutan_st,c.id_st,
            b.tgl_awal,b.tgl_akhir,b.detail_tujuan,e.nama_kabkota,f.nama_prov,
            g.nama_ppk,g.nip_ppk,g.tgl_spd,g.lokasi_spd,g.tingkat_spd,g.anggaran_instansi,h.kode_mak,h.detail_mak,
            g.trans_darat,g.trans_udara,g.trans_laut
            ')
            ->join('data_tujuan b', 'data_tertugas.id_tujuan = b.id_tujuan')
            ->join('data_st c', 'b.id_st = c.id_st')
            ->join('tabel_pegawai d', 'data_tertugas.nip_pegawai = d.nipBaru')
            ->join('tabel_kabkota e', 'b.id_kabkota = e.id_kabkota')
            ->join('tabel_prov f', 'e.id_prov = f.id_prov')
            ->join('data_spd g', 'c.id_st = g.id_st')
            ->join('tabel_mak h', 'h.id_mak = g.id_mak')
            ->where('id_tertugas', $id_tertugas)
            ->find();

        $data['tertugas'] = array_shift($data_tertugas);
        
        $data['size'] = $size;

        // print_r($data['tertugas']);
        // print_r($data);
        echo view('cetak/spd/p-spd', $data);
    }
}
