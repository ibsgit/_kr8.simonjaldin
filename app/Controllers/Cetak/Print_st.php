<?php

namespace App\Controllers\cetak;

use App\Controllers\BaseController;
use App\Models\model_st;
use App\Models\model_tujuan;
use App\Models\model_kabkota;
use App\Models\model_tertugas;
use App\Models\model_pegawai;
use App\Models\model_referensi_st;

class Print_st extends BaseController
{
    public function __construct()
    {
        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        $this->validation = \Config\Services::validation();
        $this->data['userdata'] = $this->ionAuth->user()->row();
        helper(['form', 'url', 'main', 'log']);
    }

    public function tipe1($id_st, $size = 'A4', $is_ds = false)
    {
        $model_st = new model_st();
        $model_tertugas = new model_tertugas();
        $model_tujuan = new model_tujuan();

        $data_st = $model_st->find($id_st);
        $data['st'] = $data_st;

        $data_tujuan = $model_tujuan
            ->select('CONCAT(tgl_awal," s.d. ",tgl_akhir) as periode,id_tujuan,detail_tujuan,tgl_awal,tgl_akhir,tabel_kabkota.*,tabel_prov.nama_prov')
            ->join('tabel_kabkota', 'tabel_kabkota.id_kabkota = data_tujuan.id_kabkota')
            ->join('tabel_prov', 'tabel_prov.id_prov = tabel_kabkota.id_prov')
            ->where('id_st', $id_st)
            ->findAll();

        $data['tujuan'] = $data_tujuan;
        $data_tertugas = $model_tertugas
            ->select('data_tertugas.*,tabel_pegawai.nama,tabel_pegawai.gelarDepan,tabel_pegawai.gelarBelakang')
            ->select('tabel_pegawai.nipBaru,tabel_pegawai.jenisKelamin,tabel_pegawai.pangkatAkhir,tabel_pegawai.golRuangAkhir,tabel_pegawai.jabatanNama,tabel_pegawai.satuanKerjaKerjaNama')
            ->join('tabel_pegawai', 'tabel_pegawai.nipBaru = data_tertugas.nip_pegawai')
            ->join('data_tujuan', 'data_tujuan.id_tujuan = data_tertugas.id_tujuan')
            ->where('data_tertugas.id_st', $id_st)
            ->findAll();
        $data['tertugas'] = $data_tertugas;
        $data['size'] = $size;
        $data['is_ds'] = $is_ds;
        // print_r($data);
        echo view('cetak/st/p-tipe1', $data);
    }


    public function tipe2($id_st = 22, $size = 'A4', $is_ds = false)
    {
        $model_st = new model_st();
        $model_tertugas = new model_tertugas();
        $model_tujuan = new model_tujuan();

        $data_st = $model_st->find($id_st);
        $data['st'] = $data_st;

        $data_tujuan = $model_tujuan
            ->select('CONCAT(tgl_awal," s.d. ",tgl_akhir) as periode,id_tujuan,detail_tujuan,tgl_awal,tgl_akhir,tabel_kabkota.*,tabel_prov.nama_prov')
            ->join('tabel_kabkota', 'tabel_kabkota.id_kabkota = data_tujuan.id_kabkota')
            ->join('tabel_prov', 'tabel_prov.id_prov = tabel_kabkota.id_prov')
            ->where('id_st', $id_st)
            ->findAll();

        $data['tujuan'] = $data_tujuan;
        $data_tertugas = $model_tertugas
            ->select('data_tertugas.*,tabel_pegawai.nama,tabel_pegawai.gelarDepan,tabel_pegawai.gelarBelakang')
            ->select('tabel_pegawai.nipBaru,tabel_pegawai.jenisKelamin,tabel_pegawai.pangkatAkhir,tabel_pegawai.golRuangAkhir,tabel_pegawai.jabatanNama,tabel_pegawai.satuanKerjaKerjaNama')
            ->join('tabel_pegawai', 'tabel_pegawai.nipBaru = data_tertugas.nip_pegawai')
            ->join('data_tujuan', 'data_tujuan.id_tujuan = data_tertugas.id_tujuan')
            ->where('data_tertugas.id_st', $id_st)
            ->findAll();
        $data['tertugas'] = $data_tertugas;
        $data['size'] = $size;
        $data['is_ds'] = $is_ds;
        // print_r($data);
        echo view('cetak/st/p-tipe2', $data);
    }
}
