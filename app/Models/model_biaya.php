<?php

namespace App\Models;

use CodeIgniter\Model;

class model_biaya extends Model
{
    protected $table      = 'data_biaya';
    protected $primaryKey = 'id_biaya';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_biaya', 'kode_biaya', 'nominal_biaya', 'id_tertugas',];

    protected $useTimestamps = false;
    protected $createdField  = 'created';
    protected $updatedField  = 'updated';
    protected $deletedField  = 'deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;
}
