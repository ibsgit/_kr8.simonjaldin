<?php

namespace App\Models;

use CodeIgniter\Model;

class model_tujuan extends Model
{
    protected $table      = 'data_tujuan';
    protected $primaryKey = 'id_tujuan';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_tujuan', 'detail_tujuan', 'id_kabkota', 'id_st', 'tgl_awal', 'tgl_akhir',];

    protected $useTimestamps = false;
    protected $createdField  = 'created';
    protected $updatedField  = 'updated';
    protected $deletedField  = 'deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;
}
