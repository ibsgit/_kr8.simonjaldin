<?php

namespace App\Models;

use CodeIgniter\Model;

class model_pegawai extends Model
{
    protected $table      = 'tabel_pegawai';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id', 'email', 'noHp',];

    protected $useTimestamps = false;
    protected $createdField  = 'created';
    protected $updatedField  = 'updated';
    protected $deletedField  = 'deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;
}
